
const OFF   = 0;
const WARN  = 1;
const ERROR = 2;

module.exports = {
  parserOptions : { project: './tsconfig.eslint.json' },
  plugins       : ['align-assignments', 'sort-destructure-keys'],
  extends       : ['airbnb-typescript'],
  rules         : {
    // '@typescript-eslint/indent'                      : [OFF],
    '@typescript-eslint/func-call-spacing'           : [ERROR, 'always', { allowNewlines: true }],
    '@typescript-eslint/lines-between-class-members' : [ERROR, 'always', { exceptAfterSingleLine: true }],
    '@typescript-eslint/space-before-function-paren' : [ERROR, 'always'],
    '@typescript-eslint/member-delimiter-style'      : ERROR,
    'align-assignments/align-assignments'            : [ERROR, { requiresOnly: false }],
    'curly'                                          : [ERROR, 'multi-or-nest'],
    'func-call-spacing'                              : OFF,
    'implicit-arrow-linebreak'                       : OFF,
    'import/extensions'                              : [ERROR, 'ignorePackages', {
      'js'   : 'never',
      'jsx'  : 'never',
      'mjs'  : 'never',
      'ts'   : 'never',
      'tsx'  : 'never',
      'd.ts' : 'never',
    }],
    'import/prefer-default-export' : OFF,
    'key-spacing'                  : [ERROR, {
      align: {
        afterColon  : true,
        beforeColon : true,
      },
      multiLine: {
        afterColon  : true,
        beforeColon : false,
      },
    }],
    'lines-between-class-members' : OFF,
    'max-len'                     : [OFF, 200],
    'no-confusing-arrow'          : OFF,
    'no-multi-spaces'             : OFF,
    'no-multiple-empty-lines'     : [ERROR, {
      max    : 2,
      maxBOF : 1,
    }],
    'no-nested-ternary'                : OFF,
    'no-plusplus'                      : OFF,
    'no-spaced-func'                   : OFF,
    'no-underscore-dangle'             : OFF,
    'no-unexpected-multiline'          : OFF,
    'no-use-before-define'             : OFF,
    'nonblock-statement-body-position' : OFF,
    'object-curly-newline'             : [ERROR, { multiline: true }],
    'operator-linebreak'               : [ERROR, 'before', {
      overrides: {
        '='  : 'after',
        '=>' : 'after',
      },
    }],
    'padded-blocks'                               : [ERROR, { classes: 'always' }],
    'quote-props'                                 : [ERROR, 'consistent-as-needed'],
    'sort-destructure-keys/sort-destructure-keys' : ERROR,
    'space-before-function-paren'                 : OFF,
  },
  overrides: [
    {
      files : ['*.spec.js'],
      env   : { mocha: true },
      rules : {
        'import/no-extraneous-dependencies' : OFF,
        'no-unused-expressions'             : OFF,
      },
    },
  ],
  settings: {
    'import/resolver': {
      node: {
        extensions: [
          '.js',
          '.mjs',
          '.jsx',
          '.ts',
          '.d.ts',
          '.tsx',
          '.json',
        ],
      },
    },
    'import/extensions': [
      '.js',
      '.mjs',
      '.jsx',
      '.ts',
      '.tsx',
    ],
    'import/parsers': {
      '@typescript-eslint/parser': [
        '.ts',
        '.tsx',
      ],
    },
  },
};
