
import 'jasmine';
import { assert, IsExact } from 'conditional-type-checks';

import { Ap, TypeConstructor } from './typeConstructor';
import { TRep, TypeRepresentative } from './typeRepresentative';

describe ('/hkt/typeRepresentative.ts#TypeRepresentative', () => {

  it ('should be the same as /hkt/typeRepresentative.ts#TRep', () => {
    expect (TypeRepresentative).toBe (TRep);
  });

  it ('should produce a TypeRepresentative function when called with a virtual type constructor parameter and a concrete constructor function', () => {
    // Basic type constructor that wraps its parameter into a single (1-tuple)
    interface Ctor extends TypeConstructor <[any]> {
      result: this['params'];
    }

    const TR =
      TypeRepresentative
      <Ctor> ()
      (() => (x) => [x], 1);

    expect (typeof TR).toBe ('function');
  });

  it ('should always require a constructor function', () => {
    interface Ctor extends TypeConstructor <[any]> {
      result: this['params'];
    }

    try {
      // @ts-expect-error
      TypeRepresentative <Ctor> () (1);
    } catch (error) {
      expect (error).not.toBeUndefined ();
    }
  });

  it ('should require an arity argument after the constructor function', () => {
    interface Ctor extends TypeConstructor <[any, any, any]> {
      result: this['params'];
    }

    // This should expect one more error than it is given...
    // @ts-expect-error
    TypeRepresentative
    <Ctor> ()
    (() => (x, ...rest) => [x, rest[0], rest[1]]);

    // And that parameter should be exactly 3
    TypeRepresentative
    <Ctor> ()
    // @ts-expect-error
    (() => (x, ...rest) => [x, rest[0], rest[1]], 2);

    TypeRepresentative
    <Ctor> ()
    (() => (x, ...rest) => [x, rest[0], rest[1]], 3);
  });

  it ('should require a statics value when using Statics', () => {
    interface Ctor extends TypeConstructor <[any]> {
      result: this['params'];
    }

    interface Statics extends TypeConstructor <[TypeConstructor]> {
      result: { staticProp: number };
    }

    // @ts-expect-error
    TypeRepresentative <Ctor, { Statics: Statics }> () (() => (x) => [x], 1);
  });

  it ('should allow providing a type guard function in list position with and without statics', () => {
    interface Ctor extends TypeConstructor <[any]> {
      result: this['params'];
    }

    const TR =
      TypeRepresentative <Ctor> () (
        () => (x) => [x],
        1,
        (item: any): item is Ap <Ctor, [any]> =>
          Array.isArray (item) && item.length === 1,
      );

    expect (typeof TR.is).toBe ('function');
  });

  it ("should correctly type-check the constructor function based on the type constructor's constraints", () => {
    interface Ctor extends TypeConstructor <[number]> {
      result: this['params'];
    }

    const TR =
      TypeRepresentative
      <Ctor> ()
      (() => (x) => [x], 1);

    // @ts-expect-error
    TR ('1');
    TR (1);
  });

  it ('should omit the number of specified virtual parameters from type checking of the constructor function when using VirtualParams', () => {
    interface Ctor extends TypeConstructor <[number, number, string, string]> {
      result: [this['params'][2], this['params'][3]];
    }

    const TR =
      TypeRepresentative
      <Ctor, { VirtualParams: 2 }> ()
      (() => (s1, s2) => [s1, s2], 2);

    const virtualApplied = TR <[1, 2]> ();
    virtualApplied ('1', '2');
    // @ts-expect-error
    virtualApplied (1, 2);
  });

  it ('should correctly type-check concrete statics based on the provided statics type constructor when using Statics', () => {
    interface Ctor extends TypeConstructor <[any]> {
      result: this['params'];
    }

    interface Statics extends TypeConstructor <[TypeConstructor]> {
      result: { staticProp: number };
    }

    TypeRepresentative
    <Ctor, { Statics: Statics }> ()
    (
      () => (x) => [x],
      1,
      { staticProp: 123 },
    );

    TypeRepresentative
    <Ctor, { Statics: Statics }> ()
    (
      () => (x) => [x],
      1,
      // @ts-expect-error
      { staticProp: '123' },
    );
  });

  it ('should attach statics to the type representative when configured with static properties', () => {
    interface Ctor extends TypeConstructor <[any]> {
      result: this['params'];
    }

    interface Statics extends TypeConstructor <[TypeConstructor]> {
      result: { staticProp: number };
    }

    const TR =
      TypeRepresentative
      <Ctor, { Statics: Statics }> ()
      (
        () => (x) => [x],
        1,
        { staticProp: 123 },
      );

    expect (TR.staticProp).toEqual (123);
  });

  it ('should attach the given type guard on the type representative with the key "is"', () => {
    interface Ctor extends TypeConstructor <[any]> {
      result: this['params'];
    }

    const TR =
      TypeRepresentative <Ctor> () (
        () => (x) => [x],
        1,
        (item: any): item is Ap <Ctor, [any]> =>
          Array.isArray (item) && item.length === 1,
      );

    expect (typeof TR.is).toBe ('function');
  });

  it ('should narrow the type of the argument when "is" is used as a type guard', () => {
    interface Ctor extends TypeConstructor <[any]> {
      result: this['params'];
    }

    const TR =
      TypeRepresentative <Ctor> () (
        () => (x) => [x],
        1,
        (item: any): item is Ap <Ctor, [any]> =>
          Array.isArray (item) && item.length === 1,
      );

    let item: any;
    assert <IsExact <Ap <Ctor, [any]>, typeof item>> (false);

    if (TR.is (item))
      assert <IsExact <Ap <Ctor, [any]>, typeof item>> (true);
  });

  it ('should return an instance of the type constructor when called with valid parameters', () => {
    // Basic type constructor that wraps its parameter into a single (1-tuple)
    interface Ctor extends TypeConstructor <[any]> {
      result: this['params'];
    }

    const TR =
      TypeRepresentative
      <Ctor> ()
      (() => (x) => [x], 1);

    const instance = TR (1);

    assert <IsExact <typeof instance, [number]>> (true);
    expect (instance).toEqual ([1]);
  });

  it ('should produce a correctly typed instance when using virtual parameters', () => {
    type P1P2 <P1, P2> = {
      p1?: P1;
      p2?: P2;
    };
    interface Ctor extends TypeConstructor <[number, number, string, string]> {
      result:
      & P1P2 <this['params'][0], this['params'][1]>
      & [this['params'][2], this['params'][3]];
    }

    const TR =
      TypeRepresentative
      <Ctor, { VirtualParams: 2 }> ()
      (() => (s1, s2) => [s1, s2], 2);

    const instance =
      TR
      <[1, 2]> ()
      ('1', '2');

    expect (instance).toEqual (['1', '2']);
    assert <IsExact <typeof instance, ['1', '2'] & P1P2 <1, 2>>> (true);
  });

  it ('should auto-curry for virtual parameters', () => {
    interface Ctor extends TypeConstructor <[number, number, string, string]> {
      result: [this['params'][2], this['params'][3]];
    }

    const TR =
      TypeRepresentative
      <Ctor, { VirtualParams: 2 }> ()
      (() => (s1, s2) => [s1, s2], 2);

    const partiallyApplied = TR <[1]> ();

    expect (typeof partiallyApplied).toBe ('function');
  });

  it ('should auto-curry for concrete parameters', () => {
    interface Ctor extends TypeConstructor <[number, number, string, string]> {
      result: [this['params'][2], this['params'][3]];
    }

    const TR =
      TypeRepresentative
      <Ctor, { VirtualParams: 2 }> ()
      (() => (s1, s2) => [s1, s2], 2);

    const partiallyApplied = TR <[1, 2]> () ('1');

    expect (typeof partiallyApplied).toBe ('function');
  });

  it ("should correctly type check virtual parameters based on the type constructor's constraint", () => {
    interface Ctor extends TypeConstructor <[number, number, string, string]> {
      result: [this['params'][2], this['params'][3]];
    }

    const TR =
      TypeRepresentative
      <Ctor, { VirtualParams: 2 }> ()
      (() => (s1, s2) => [s1, s2], 2);

    // @ts-expect-error
    TR <[1, '2']> ();
  });

  it ("should correctly type check concrete parameters based on the type constructor's constraint", () => {
    interface Ctor extends TypeConstructor <[number, number, string, string]> {
      result: [this['params'][2], this['params'][3]];
    }

    const TR =
      TypeRepresentative
      <Ctor, { VirtualParams: 2 }> ()
      (() => (s1, s2) => [s1, s2], 2);

    // @ts-expect-error
    TR <[1, 2]> () ('1', 2);
  });

  it ("should assign the 'constructor' property of all instances to the resulting TRep", () => {
    interface Ctor extends TypeConstructor <[number, number]> {
      result: [this['params']['0'], this['params']['1']];
    }

    const TR =
      TypeRepresentative
      <Ctor> ()
      (() => (n1, n2) => [n1, n2], 2);

    const instance = TR (1, 2);

    expect (instance.constructor).toEqual (TR);
  });

});
