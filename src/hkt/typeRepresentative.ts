
import { ApI, ApP, TCtor } from './typeConstructor';
import { Cast, Diff, DropN, IsTuple, TakeN } from '../util';
import { curry } from './curry';

/* eslint-disable @typescript-eslint/indent */
type AcceptVirtual <
  Instance extends TCtor,
  Statics extends TCtor <[TCtor], { [key: string]: any }>,
  Constructor extends () => (...args: any[]) => any,
  VirtualParamCount extends number,
> = (
  <Params extends ApI <
    Instance['partialParams'],
    [TakeN <VirtualParamCount, Instance['paramConstraint']>]>
  > () => (
    TypeRepresentative <ApP <Instance, Params>, Statics, Diff <VirtualParamCount, Params['length']>, Constructor>
  )
);

type AcceptConcrete <
  Instance extends TCtor,
  Statics extends TCtor <[TCtor], { [key: string]: any }>,
  Constructor extends () => (...args: any[]) => any,
  VirtualParamCount extends number,
> = (
  <Params extends ApI <
    Instance['partialParams'],
    [Instance['paramConstraint']]
  >> (...params: Params) => (
    IsTuple <Instance['paramConstraint']> extends false
      ? ApI <Instance, Params>
    : DropN <Params['length'], Instance['paramConstraint']> extends [any, ...any[]]
      ? TypeRepresentative <ApP <Instance, Params>, Statics, VirtualParamCount, Constructor>
      : ApI <Instance, Params>
  )
);

type TypeRepresentativeProperties <
  Instance extends TCtor,
  Statics extends TCtor <[TCtor]>,
  Constructor extends () => (...args: any[]) => any,
  VirtualParamCount extends number,
> = (
  & ApI <Statics, [Instance]>
  & {
    is?: (item: any) => item is ApI <Instance, Instance['paramConstraint']>;
    _ConstructorFunction: Constructor;

    _Instance?: Instance;
    _Statics?: Statics;
    _VirtualParamCount?: VirtualParamCount;
  }
);

export type TypeRepresentative <
  Instance extends TCtor,
  Statics extends TCtor <[TCtor], { [key: string]: any }> = EmptyStatics,
  VirtualParamCount extends number = 0,
  Constructor extends ConcreteConstructorFor <Instance, VirtualParamCount> = ConcreteConstructorFor <Instance, VirtualParamCount>,
> = (
  & TypeRepresentativeProperties <Instance, Statics, Constructor, VirtualParamCount>
  & (
    VirtualParamCount extends 0
      ? AcceptConcrete <Instance, Statics, Constructor, VirtualParamCount>
      : AcceptVirtual <Instance, Statics, Constructor, VirtualParamCount>
  )
);

export type AnyTypeRepresentative = TypeRepresentative <TCtor>;

export interface EmptyStatics extends TCtor <[TCtor], { [key: string]: any }> { result: {} }

type VirtualConstructorArgs <Instance extends TCtor, VirtualParamCount extends number> = (
  TakeN <VirtualParamCount, Instance['paramConstraint']>
);
type ConcreteConstructorArgs <Instance extends TCtor, VirtualParamCount extends number> = (
  DropN <VirtualParamCount, Instance['paramConstraint']>
);
type ConcreteConstructorFor <Instance extends TCtor, VirtualParamCount extends number = 0> = (
  // TODO: Find a way to allow the user to provide a concrete-only constructor for concrete-only TypeConstructors
  // VirtualParamCount extends 0
  //   ? <P extends ConcreteConstructorArgs <Instance, VirtualParamCount>> (...params: P) => ApI <Instance, DropN <VirtualParamCount, Instance['paramConstraint']>>
  //   : <V extends VirtualConstructorArgs <Instance, VirtualParamCount>> () => <P extends ConcreteConstructorArgs <Instance, VirtualParamCount>> (...params: P) => ApI <Instance, DropN <VirtualParamCount, Instance['paramConstraint']>>
  <V extends VirtualConstructorArgs <Instance, VirtualParamCount>> () => <P extends ConcreteConstructorArgs <Instance, VirtualParamCount>> (...params: P) => ApI <Instance, DropN <VirtualParamCount, Instance['paramConstraint']>>
);

type GuardFunctionFor <Instance extends TCtor> = (
  (item: any) => item is ApI <Instance, Instance['paramConstraint']>
);

type OptionalPropertyOf <Prop extends string, Default, Container> = (
  Prop extends keyof Container
    ? Container[Prop]
    : Default
);

// Unfortunately, these conditions can't be broken out into separate types.
// TS complains about tuples not all having names when trying to spread the
//   result of another type into a named tuple.
type ParamsFor <
  Instance extends TCtor,
  Statics extends TCtor <[TCtor]>,
  ConcreteParams extends number,
  ConcreteConstructor extends () => (...args: any[]) => any,
  GuardFunction extends (...args: any[]) => any,
> = (
  EmptyStatics extends Statics
  ? [concreteConstructor: ConcreteConstructor, concreteParams: ConcreteParams, guardFunction?: GuardFunction]
  : [concreteConstructor: ConcreteConstructor, concreteParams: ConcreteParams, statics: ApI <Statics, [Instance]>, guardFunction?: GuardFunction]
);

export function TypeRepresentative <
  Instance extends TCtor,
  // Allow configuration with optional parameters
  Configuration extends {
    Statics?: TCtor <[TCtor], { [key: string]: any }>;
    VirtualParams?: number;
  } = {},
  // Assign defaults when optional parameters are not specified
  Statics extends TCtor <[TCtor], { [key: string]: any }> =
    Cast <
      OptionalPropertyOf <'Statics', EmptyStatics, Configuration>,
      TCtor <[TCtor], { [key: string]: any }>
    >,
  VirtualParams extends number =
    Cast <
      OptionalPropertyOf <'VirtualParams', 0, Configuration>,
      number
    >,
> (): (
  <
    ConcreteConstructor extends ConcreteConstructorFor <Instance, VirtualParams>,
    GuardFunction extends GuardFunctionFor <Instance>,
    ConcreteParams extends Diff <Instance['paramConstraint']['length'], VirtualParams>
  >
  (...params: ParamsFor <
    Instance,
    Statics,
    ConcreteParams,
    ConcreteConstructor,
    GuardFunction
  >) => (
    TypeRepresentative <
      Instance,
      Statics,
      VirtualParams,
      ConcreteConstructor
    >
  )
) {

  const withVirtualParams =
    <
      ConcreteConstructor extends ConcreteConstructorFor <Instance, VirtualParams>,
      GuardFunction extends GuardFunctionFor <Instance>,
      ConcreteParams extends Diff <Instance['paramConstraint']['length'], VirtualParams>
    >
    (...params: ParamsFor <
      Instance,
      Statics,
      ConcreteParams,
      ConcreteConstructor,
      GuardFunction
    >) => {

      /**
       * The implementation of this function does not need to account for virtual
       * type parameters at runtime, as they will not exist. Basic auto-currying takes
       * care of accepting parameters in order, as well as calling without parameters
       * (basically what happens when applying virtual type parameters)
       *
       * Implementation is basically to curry the given function and return that with
       * new types forcibly attached.
       */

      const [concreteConstructor, concreteParams, staticsOrGuardFunction, guardFunction] = params;

      if (typeof concreteConstructor !== 'function')
        throw new Error ('concreteConstructor must be a function');

      // Call once to before currying so that runtime behavior matches type level behavior
      const takeConcrete          = concreteConstructor ();
      let typeRep: any            = null;
      const withAssignConstructor =
        <P extends DropN<VirtualParams, Instance['paramConstraint']>>
        (...args: P) => {
          const inst = takeConcrete (...args);
          Object.defineProperty (inst, 'constructor', { value: typeRep, enumerable: false });
          return inst;
        };

      // Assign length for currying. This ensures support for rest syntax in concrete
      // constructor
      Object.defineProperty (withAssignConstructor, 'length', { value: concreteParams });

      typeRep = curry (withAssignConstructor);
      Object.defineProperty (typeRep, 'name', { value: concreteConstructor.name });

      // No statics provided, but type guard function provided
      if (typeof staticsOrGuardFunction === 'function')
        typeRep.is = staticsOrGuardFunction;
      else if (typeof guardFunction === 'function')
        typeRep.is = guardFunction;

      // Statics provided
      if (
        staticsOrGuardFunction !== undefined
        && staticsOrGuardFunction !== null
        && typeof staticsOrGuardFunction === 'object'
      )
        return Object.assign (typeRep, staticsOrGuardFunction);

      return typeRep as (
        TypeRepresentative <
          Instance,
          Statics,
          VirtualParams,
          ConcreteConstructor
        >
      );
    };

  return withVirtualParams as (
    <
      ConcreteConstructor extends ConcreteConstructorFor <Instance, VirtualParams>,
      GuardFunction extends GuardFunctionFor <Instance>,
      ConcreteParams extends Diff <Instance['paramConstraint']['length'], VirtualParams>
    >
    (...params: ParamsFor <
      Instance,
      Statics,
      ConcreteParams,
      ConcreteConstructor,
      GuardFunction
    >) => (
      TypeRepresentative <
        Instance,
        Statics,
        VirtualParams,
        ConcreteConstructor
      >
    )
  );

}

export const isTypeRepresentative = (item: any): item is AnyTypeRepresentative => (
  typeof item === 'function'
  && typeof item.is === 'function'
);

// == Aliases ==

export type TRep <
  I extends TCtor,
  S extends TCtor <[TCtor], { [key: string]: any }> = EmptyStatics,
  C extends () => (...args: any[]) => any = () => (...args: any[]) => any,
  VPC extends number = 0,
> = TypeRepresentative <I, S, VPC, C>;

export type AnyTRep = TRep <TCtor>;
export const TRep = TypeRepresentative;
export const isTRep: (item: any) => item is AnyTRep = isTypeRepresentative;

// == Test ==

/*

type KV <K extends string, V> = { [k in K]?: V };
interface MyTCtor extends TCtor <[string, number]> {
  result: [this['params']['1'], this['params']['0']?];
}

type CC = ConcreteConstructorFor <MyTCtor, 1>;

const cc: CC =
<Virtual extends [number]> () =>
  (n) =>
    [n] as [typeof n, Virtual[0]?];

const MyTRep =
  TRep
  <MyTCtor, { VirtualParams: 1 }> ()
  (
    <Virtual extends [number]> () =>
      (n) =>
        [n] as [typeof n, Virtual[0]?],
    1,
    // (item): item is ApI <MyTCtor, [string, number]> => Array.isArray (item) && item.length === 2 && item.every ((n) => typeof n === 'number'),
  );

const inst = MyTRep <['xyz']> () (123);

*/
