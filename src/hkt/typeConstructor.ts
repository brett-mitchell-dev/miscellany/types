
import { Cast, PartialTuple as CalcPartialTuple, RelaxedTuple, Tail, Init, Last, Last2, IsTuple } from '../util';


// == Type functions and application of type parameters ==

export interface TypeConstructorBase <ParamConstraint = unknown, ReturnConstraint = unknown> {
  paramConstraint: ParamConstraint;
  params: this['apply'] extends this['paramConstraint']
    ? this['apply'] : this['paramConstraint'];
  apply: unknown;
  result: ReturnConstraint;

  mergeParams: TypeConstructorBase <[any, any]>;
  diffParams: TypeConstructorBase <[any, any]>;
  paramsSatisfied: TypeConstructorBase <[any, any]>;
  partialParams: TypeConstructorBase <[any]>;
}

export interface MergeTuples extends TypeConstructorBase <[any[], any[]]> {
  result: [...this['params'][0], ...this['params'][1]];
}

export interface DiffTuples extends TypeConstructorBase <[any[], any[]]> {
  result: this['params'][1] extends [...RelaxedTuple <this['params'][0]>, ...infer Rest] ? Rest : never;
}

export interface LengthEqual extends TypeConstructorBase <[any[], any[]]> {
  result: this['params'][0]['length'] extends this['params'][1]['length'] ? true : false;
}

export interface PartialTuple extends TypeConstructorBase <[any[]]> {
  result: CalcPartialTuple <this['params'][0]>;
}

export interface TypeConstructor <ParamConstraint extends any[] = any[], ReturnConstraint = unknown> extends TypeConstructorBase <ParamConstraint, ReturnConstraint> {
  mergeParams: MergeTuples;
  diffParams: DiffTuples;
  paramsSatisfied: LengthEqual;
  partialParams: PartialTuple;
}

export type ApplyImmediate <
  Kind extends TypeConstructorBase <any>,
  Params extends Kind['paramConstraint'],
> = (Kind & { apply: Params })['result'];

export interface ApplyPartial <
  Kind extends TypeConstructorBase <any>,
  Params extends ApplyImmediate <Kind['partialParams'], [Kind['paramConstraint']]>,
> extends TypeConstructorBase <ApplyImmediate <Kind['diffParams'], [Params, Kind['paramConstraint']]>> {
  result: ApplyImmediate <Kind, ApplyImmediate <Kind['mergeParams'], [Params, this['params']]>>;

  mergeParams: Kind['mergeParams'];
  diffParams: Kind['diffParams'];
  paramsSatisfied: Kind['paramsSatisfied'];
  partialParams: Kind['partialParams'];
}

export type Apply <
  Kind extends TypeConstructorBase,
  Params extends ApplyImmediate <Kind['partialParams'], [Kind['paramConstraint']]>,
> = (
  ApplyImmediate <Kind['paramsSatisfied'], [Params, Kind['paramConstraint']]> extends true
    ? ApplyImmediate <Kind, Params>
    : ApplyPartial <Kind, Params>
);

type FoldFront <TypeConstructors extends TypeConstructor [], Acc extends TypeConstructors[0]['paramConstraint']> = {
  0: [TypeConstructors[0]['result']] extends TypeConstructors[1]['paramConstraint']
    ? FoldFront <Tail <TypeConstructors>, [ApplyImmediate <TypeConstructors[0], Acc>]>
    : never;
  1: FoldFront <Tail <TypeConstructors>, [ApplyImmediate <TypeConstructors[0], Acc>]>;
  2: Acc[0];
}[TypeConstructors['length'] extends 0 ? 2 : TypeConstructors['length'] extends 1 ? 1 : 0];

export interface Pipe <TypeConstructors extends TypeConstructor []>
  extends TypeConstructor <TypeConstructors[0]['paramConstraint'], Last <TypeConstructors> ['result']> {
  result: FoldFront <TypeConstructors, this['params']>;
}


type FoldBack <TypeConstructors extends TypeConstructor [], Acc extends Last <TypeConstructors> ['paramConstraint']> = {
  0: [Last <TypeConstructors> ['result']] extends Last2 <TypeConstructors> ['paramConstraint']
    ? FoldFront <Cast <Init <TypeConstructors>, TypeConstructor []>, [ApplyImmediate <Last <TypeConstructors>, Acc>]>
    : never;
  1: FoldBack <Cast <Init <TypeConstructors>, TypeConstructor []>, [ApplyImmediate <Last <TypeConstructors>, Acc>]>;
  2: Acc[0];
}[TypeConstructors['length'] extends 0 ? 2 : TypeConstructors['length'] extends 1 ? 1 : 0];

export interface Compose <TypeConstructors extends TypeConstructor []>
  extends TypeConstructor <Last <TypeConstructors> ['paramConstraint'], TypeConstructors[0]['result']> {
  result: FoldBack <TypeConstructors, this['params']>;
}

export interface IsTypeConstructorOf extends TypeConstructor <[any, any]> {
  result: this['params']['1'] extends TypeConstructorBase
    ? this['params']['1']['paramsConstraint'] extends this['params']['0']
      ? true
      : false
    : false;
}

export type IsTypeConstructorOfInline <ParamConstraint, Constructor> = (
  Constructor extends TypeConstructorBase
    ? Constructor['paramConstraint'] extends ParamConstraint
      ? true
      : false
    : false
);

// == Aliases ==

export type TCtorBase <C = unknown, R = unknown> = TypeConstructorBase <C, R>;
export type TCtor <P extends any[] = any[], R = unknown> = TypeConstructor <P, R>;
export type ApI <T extends TCtorBase <any>, P extends T['paramConstraint']> = ApplyImmediate <T, P>;
export type ApP <T extends TCtorBase <any>, P extends T['paramConstraint']> = ApplyPartial <T, P>;
export type Ap <T extends TCtorBase <any>, P extends ApI <T['partialParams'], [T['paramConstraint']]>> = Apply <T, P>;
export type IsTCtorOf = IsTypeConstructorOf;
export type IsTCtorOfInline <P, C> = IsTypeConstructorOfInline <P, C>;

// == Test ==

/*

interface T1 extends TypeConstructor <[any, string, boolean]> { result: this['params'] }

// Directly applied with all required type parameters
type FullyApplied1 = Apply <T1, ['foo', 'bar', true]>;
// => ['foo', 'bar', true]

// Apply one type parameter at a time
type P1 = Apply <T1, [number]>;
// => ApplyPartial<T1, [number]>
type P2 = Apply <P1, ['123']>;
// => ApplyPartial<ApplyPartial<T1, [number]>, ["123"]>
type FullyApplied2 = Apply <P2, [true]>;
// => [number, '123', true]

// Apply two type parameters at a time
type P3 = ApplyPartial <T1, [number, '123']>;
type FullyApplied3 = Apply <P3, [true]>;
// => [number, '123', true]

// Restricts parameters based on provided constraint
type P4 = ApplyPartial <T1, [number, '123', true, 'extra']>;
// Type '[number, "123", true, "extra"]' does not satisfy the constraint '[any?, string?, boolean?]'.
//   Source has 4 element(s) but target allows only 3.

// Partially applied type constructors also have valid constraints
type P5 = ApplyPartial <T1, [number]>;
type P6 = ApplyPartial <P5, ['123', 'not a boolean']>;
// Type '["123", "not a boolean"]' does not satisfy the constraint '[string?, boolean?]'.
//   Types of property '1' are incompatible.
//     Type 'string' is not assignable to type 'boolean'.

interface T2 extends TCtor <[number, number], string> { result: `${this['params'][0]} - ${this['params'][1]}` }
interface T3 extends TCtor <[string], [string, 123]> { result: [this['params'][0], 123] }

type T4 = Compose <[T3, T2]>;
type FullyApplied4 = Apply <T4, [123, 456]>;
type P7 = Apply <T4, [123]>;
type FullyApplied5 = Apply <P7, [123]>;
type FullyApplied6 = Apply <T4, [123, 789]>;

// Test custom partial application

interface CustomParamsType <X, Y> {
  x: X;
  y: Y;
}

interface MergeParams extends TypeConstructorBase <[Partial <CustomParamsType <any, any>>, Partial <CustomParamsType <any, any>>]> {
  result: this['params'][0] & this['params'][1];
}

type DiffCustomParams <Given extends Partial <CustomParamsType <any, any>>, Constraint extends Partial <CustomParamsType <any, any>>> = (
  { [K in Exclude <keyof Constraint, keyof Given>]: Constraint[K] }
);
interface DiffParams extends TypeConstructorBase <[Partial <CustomParamsType <any, any>>, Partial <CustomParamsType <any, any>>]> {
  // result: [this['params'][0], this['params'][1]];
  result: DiffCustomParams <this['params'][0], this['params'][1]>;
}

interface AllPropsPresent extends TypeConstructorBase <[Partial <CustomParamsType <any, any>>, Partial <CustomParamsType <any, any>>]> {
  //      keyof Given params      extends keyof Params constraint
  result: keyof this['params'][1] extends keyof this['params'][0] ? true : false;
}

interface PartialParams extends TypeConstructorBase <[Partial <CustomParamsType <any, any>>]> {
  result: Partial <this['params'][0]>;
}

interface CustomTypeConstructor <
  ParamConstraint extends CustomParamsType <any, any> = CustomParamsType <any, any>,
  ReturnConstraint = unknown
> extends TypeConstructorBase <ParamConstraint, ReturnConstraint> {
  mergeParams: MergeParams;
  diffParams: DiffParams;
  paramsSatisfied: AllPropsPresent;
  partialParams: PartialParams;
}

interface UsesCustomTypeConstructor extends CustomTypeConstructor <{ x: number; y: string }, string> {
  result: `${this['params']['x']} ${this['params']['y']}`;
}

type FullyApplied = Ap <UsesCustomTypeConstructor, { x: 123; y: 'Hello world!' }>;
// => "123 Hello world!"

type PartiallyApplied = Ap <UsesCustomTypeConstructor, { x: 123 }>;
type IncorrectlyApplied = Ap <PartiallyApplied, { y: 123 }>;
// Type '{ y: 123; }' does not satisfy the constraint 'Partial<DiffCustomParams<{ x: 123; }, { x: number; y: string; }>>'.
//   Types of property 'y' are incompatible.
//     Type 'number' is not assignable to type 'string'.
type EventuallyApplied = Ap <PartiallyApplied, { y: 'Hello world!' }>;
// => "123 Hello world!"

*/

/* eslint-disable @typescript-eslint/indent */

/*

type MapTo <FunctorType extends TCtor <any[]>, Param, Return> = (
  <
    E extends [...Init <FunctorType['paramConstraint']>, Param],
    // No need for arity-specific MapTo cases. Just infer leading terms here.
    // Functors are not restricted to any set number of parameters this way.
    // Coercion could be done with a conditional type within the ApI below, but
    //  I thought constraining it with a default was a bit cleaner.
    WithReturn extends FunctorType['paramConstraint'] = [...Init <E>, Return],
  >
  (functorInstance: ApI <FunctorType, E>) =>
    ApI <FunctorType, WithReturn>
);

interface Functor <FunctorType extends TCtor <any[]>> {
  map:
    <Param, Return>
    (fn: (param: Param) => Return) =>
      MapTo <FunctorType, Param, Return>;
}

function lift
  <FunctorType extends TCtor <any[]>>
  (f: Functor <FunctorType>)
  : <P, R> (fn: (p: P) => R) => MapTo <FunctorType, P, R> {
    return f.map;
  }


namespace E {

  export interface Left <A> {
    readonly tag: 'Left';
    readonly left: A;
  }
  export interface Right <A> {
    readonly tag: 'Right';
    readonly right: A;
  }

  export type Either <A, B> = Left <A> | Right <B>;
  export interface EitherCtor extends TCtor <[any, any]> {
    result: Either <this['params'][0], this['params'][1]>;
  }

  export const left =  <V> (v: V): Left <V> =>  ({ tag: 'Left',  left: v  });
  export const right = <V> (v: V): Right <V> => ({ tag: 'Right', right: v });


  export const functor: Functor <EitherCtor> = {
    map:
      <Param, Return> (fn: (param: Param) => Return) =>
      <L> (eitherInstance: Either <L, Param>)
      : Either <L, Return> => (
        eitherInstance.tag === 'Left'
          ? eitherInstance
          : right (fn (eitherInstance.right))
      ),
  };
}

const mapLen = E.functor.map ((s: string) => s.length);

const r1 = mapLen (E.right (123));
// Argument of type 'Right<number>' is not assignable to parameter of type 'Either<any, string>'.
//   Type 'Right<number>' is not assignable to type 'Right<string>'.
//     Type 'number' is not assignable to type 'string'.

const r2 = mapLen (E.right ('123'));
// => Either <any, number>
const r3 = mapLen (E.left (123));
// => Either <any, number>


const liftedMapLen = lift (E.functor) ((s: string) => s.length);

// Not totally sure why the last error line is not present here as it is above.
const r4 = mapLen (E.right (123));
// Argument of type 'Right<number>' is not assignable to parameter of type 'Either<any, string>'.
//   Type 'Right<number>' is not assignable to type 'Right<string>'.

const r5 = mapLen (E.right ('123'));
// => Either <any, number>
const r6 = mapLen (E.left (123));
// => Either <any, number>

*/
