
import { assert, IsExact, IsNever } from 'conditional-type-checks';

import { TCtor, Ap, Pipe, Compose } from './typeConstructor';

export namespace TestApply {

  // == Basic application ==
  interface TCtor1 extends TCtor <[any]> {
    result: [this['params'][0]];
  }
  type Expected1 = [number];

  assert <IsExact <Ap <TCtor1, [number]>, Expected1>> (true);


  // == Partial application ==
  interface TCtor2 extends TCtor <[any, any, any]> {
    result: this['params'];
  }
  type Expected2 = [number, string, boolean];

  type PartialA = Ap <TCtor2, [number, string]>;

  type PartialB1 = Ap <TCtor2, [number]>;
  type PartialB2 = Ap <PartialB1, [string]>;

  type FullyApplied = Ap <TCtor2, [number, string, boolean]>;

  // Base case
  assert <IsExact <FullyApplied, Expected2>> (true);
  // Partially applied with two call
  assert <IsExact <Ap <PartialA, [boolean]>, Expected2>> (true);
  // Partially applied with three calls
  assert <IsExact <Ap <PartialB2, [boolean]>, Expected2>> (true);
  // Apply multiple type parameters to partially applied type constructor
  assert <IsExact <Ap <PartialB1, [string, boolean]>, Expected2>> (true);


  // == Parameter constraints ==
  interface TCtor3 extends TCtor <[string, number]> {
    result: this['params'];
  }

  // Should permit any [string, number] params
  assert <IsExact <Ap <TCtor3, [string, number]>, [string, number]>> (true);
  assert <IsExact <Ap <TCtor3, ['foo', number]>, ['foo', number]>> (true);
  assert <IsExact <Ap <TCtor3, [string, 123]>, [string, 123]>> (true);
  assert <IsExact <Ap <TCtor3, ['foo', 123]>, ['foo', 123]>> (true);


  // == Return constraints ==
  interface TCtor4 extends TCtor <[], string> {
    result: 'foo';
  }

  // Should restrict return type to be string
  assert <IsExact <Ap <TCtor4, []>, 'foo'>> (true);

}

export namespace TestPipe {

  // == Piped type constructors may be applied ==
  interface PipeA1 extends TCtor <[string, string], string> {
    result: `${this['params'][0]} ${this['params'][1]} 1`;
  }
  interface PipeA2 extends TCtor <[string], string> {
    result: `${this['params'][0]} 2`;
  }

  type PipedA = Pipe <[PipeA1, PipeA2]>;

  assert <IsExact <Ap <PipedA, ['P1', 'P2']>, 'P1 P2 1 2'>> (true);


  // == Piped type constructors may be partially applied ==
  type PipedAPartial = Ap <PipedA, ['P1']>;

  assert <IsExact <Ap <PipedAPartial, ['P2']>, 'P1 P2 1 2'>> (true);


  // == Incorrect composition results in never when applied ==
  interface PipeB1 extends TCtor <[], string> {
    result: 'foo';
  }
  interface PipeB2 extends TCtor <[number], number> {
    result: this['params'][0];
  }

  type PipedB = Pipe <[PipeB1, PipeB2]>;

  assert <IsNever <Ap <PipedB, []>>> (true);


  // == Piped type constructor has params of first provided composing element ==
  interface PipeC1 extends TCtor <[string, number], string> {
    result: `${this['params'][0]} ${this['params'][1]}`;
  }
  interface PipeC2 extends TCtor <[string], number> {
    result: this['params'][0]['length'];
  }

  type PipedC = Pipe <[PipeC1, PipeC2]>;

  assert <IsExact <PipedC['paramConstraint'], [string, number]>> (true);


  // == Piped type constructor has return type of last provided composing element ==
  assert <IsExact <PipedC['result'], number>> (true);

}

export namespace TestCompose {

  // == Piped type constructors may be applied ==
  interface CompA1 extends TCtor <[string, string], string> {
    result: `${this['params'][0]} ${this['params'][1]} 1`;
  }
  interface CompA2 extends TCtor <[string], string> {
    result: `${this['params'][0]} 2`;
  }

  type ComposedA = Compose <[CompA2, CompA1]>;

  assert <IsExact <Ap <ComposedA, ['P1', 'P2']>, 'P1 P2 1 2'>> (true);


  // == Piped type constructors may be partially applied ==
  type PipedAPartial = Ap <ComposedA, ['P1']>;

  assert <IsExact <Ap <PipedAPartial, ['P2']>, 'P1 P2 1 2'>> (true);


  // == Incorrect composition results in never when applied ==
  interface CompB1 extends TCtor <[], string> {
    result: 'foo';
  }
  interface CompB2 extends TCtor <[number], number> {
    result: this['params'][0];
  }

  type PipedB = Compose <[CompB2, CompB1]>;

  assert <IsNever <Ap <PipedB, []>>> (true);


  // == Piped type constructor has params of first provided composing element ==
  interface CompC1 extends TCtor <[string, number], string> {
    result: `${this['params'][0]} ${this['params'][1]}`;
  }
  interface CompC2 extends TCtor <[string], number> {
    result: this['params'][0]['length'];
  }

  type PipedC = Compose <[CompC2, CompC1]>;

  assert <IsExact <PipedC['paramConstraint'], [string, number]>> (true);


  // == Piped type constructor has return type of last provided composing element ==
  assert <IsExact <PipedC['result'], number>> (true);

}
