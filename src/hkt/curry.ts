
import { Curry, DropN, Cast } from '../util';

export const curry =
  <P extends any[], R> (fn: (...params: P) => R): Curry <P, R> => {
    const curried = <T extends any[]> (...outer: Cast <T, Partial <P>>) => {
      if (fn.length > outer.length) {
        const next = (...inner) => {
          const args: P = [...outer, ...inner] as P;
          return fn (...args);
        };

        Object.defineProperty (next, 'name', { value: fn.name });
        Object.defineProperty (next, 'length', { value: fn.length - outer.length });

        return curry <DropN <T['length'], P>, R> (next);
      }

      return fn (...(outer as unknown as P));
    };

    Object.defineProperty (curried, 'name', { value: fn.name });
    Object.defineProperty (curried, 'length', { value: fn.length });

    return curried as unknown as Curry <P, R>;
  };
