
const INVALID_TAG: unique symbol = Symbol ('INVALID_TAG');

// Credit: https://github.com/microsoft/TypeScript/pull/40468#issuecomment-727634278
export type Invalid <T extends { [key: string]: any } = {}> = {
  [Key in keyof T | typeof INVALID_TAG]: (
    Key extends keyof T
      ? T[Key]
      : typeof INVALID_TAG
  );
};
