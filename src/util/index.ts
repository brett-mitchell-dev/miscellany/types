
export type Cast <A, B> = A extends B ? A : B;

// Credit: https://github.com/microsoft/TypeScript/issues/26223#issuecomment-674514787
type BuildPowersOf2LengthArrays<N extends number, R extends never[][]> =
  R[0][N] extends never ? R : BuildPowersOf2LengthArrays<N, [[...R[0], ...R[0]], ...R]>;

type ConcatLargestUntilDone<N extends number, R extends never[][], B extends never[]> =
  B['length'] extends N ? B : [...R[0], ...B][N] extends never
    ? ConcatLargestUntilDone<N, R extends [R[0], ...infer U] ? U extends never[][] ? U : never : never, B>
    : ConcatLargestUntilDone<N, R extends [R[0], ...infer U] ? U extends never[][] ? U : never : never, [...R[0], ...B]>;

type Replace<R extends any[], T> = { [K in keyof R]: T };

export type TupleOf<T, N extends number> = number extends N ? T[] : {
  [K in N]:
  BuildPowersOf2LengthArrays<K, [[never]]> extends infer U ? U extends never[][]
    ? Replace<ConcatLargestUntilDone<K, U, []>, T> : never : never;
}[N];

export type DropN <N extends number, L extends any[]> =
  L extends [...TupleOf <any, N>, ...infer Rest] ? Rest : never;

export type TakeN <N extends number, L extends any[]> =
  L extends [...infer FirstN, ...DropN <N, L>] ? FirstN : never;

export type Nth <N extends number, T extends any[] | readonly any[]> = T[N];
export type Head <T extends any[] | readonly any[]> =
  T['length'] extends 0 ? never : T[0];
export type Tail <T extends any[] | readonly any[]> =
((...t: T) => any) extends ((head: any, ...tail: infer TTail) => any)
  ? TTail
  : never;

export type NthFromEnd <N extends number, T extends any[] | readonly any[]> =
  [...TupleOf <any, N>, ...T][T['length']];


export type Last <T extends any[] | readonly any[]> =
  T['length'] extends 0 ? never : [any, ...T][T['length']];
export type Init <T extends any[] | readonly any[]> =
  T extends [...infer I, infer L] ? I : never;
export type Last2 <T extends any[] | readonly any[]> =
  T['length'] extends 0 ? never : T['length'] extends 1 ? never : [any, any, ...T][T['length']];

export type MapProp <
  P extends string,
  T extends any[] | { [key: string]: any },
> = { [K in keyof T]: P extends keyof T[K] ? T[K][P] : never };

export type Constructor <Params extends any[], Return> = { new (...args: Params): Return };
export type ConstructorParameters <T extends { new (...args: any[]): any }> = (
  T extends { new (...args: infer Parameters): any }
    ? Parameters
    : never
);
export type ConstructorReturnType <T extends { new (...args: any[]): any }> = (
  T extends { new (...args: any[]): infer Return }
    ? Return
    : never
);

export type ConcatAllStrings <As extends string[][], ACC extends string[] = []> = {
  0: ACC;
  1: ConcatAllStrings <Tail <As>, [...ACC, ...Head <As>]>;
}[As['length'] extends 0 ? 0 : 1];

export type Default <A, B> = [A] extends [never] ? B : A;
export type Cond <Condition, IfTrue, IfFalse> = Condition extends true ? IfTrue : IfFalse;

/**
 * Procedure:
 *  1. If the parameter is not of Array type, return false. This guarantees that the type
 *      of T['length'] is at least 'number' in subsequent steps.
 *  2. Check to see if 'number' extends the length of the parameter
 *    - If yes, the parameter is just an array (i.e. it's length property is exactly 'number')
 *    - If no, the parameter is specifically a tuple (i.e. it's length property is more
 *      specific than the guaranteed 'number' constraint already established above)
 */
export type IsTuple <T> = (
  T extends any[]
    ? number extends T['length']
      ? false
      : true
    : false
);

type CoerceArray <T> = T extends any[] ? T : never;
export type PartialTuple <T extends any[]> = { [K in keyof T]?: T[K] };
export type RelaxedTuple <T extends any[]> = CoerceArray <{ [K in keyof T]: any }>;

type NaiveDiff <N1 extends number, N2 extends number> = (
  [...TupleOf <any, N1>] extends [...TupleOf <any, N2>, ...infer Rest]
    ? Rest['length']
    : never
);
export type Gt <N1 extends number, N2 extends number> = (
  NaiveDiff <N1, N2> extends 0
    ? false
    : true
);
// TODO: If there is a trick for getting negative numbers, use it.
//       If the only solution is to store a negation map, skip this implementation
//        as this repo only needs NaiveDiff for tuple lengths.
export type Negate <N extends number> = never;
export type Diff <N1 extends number, N2 extends number> =
  Gt <N1, N2> extends true
    ? NaiveDiff <N1, N2>
    : 0;
    // : Negate <Diff <N2, N1>>;

// Credit for this goes to millsp and his awesome article on currying:
//  https://www.freecodecamp.org/news/typescript-curry-ramda-types-f747e99744ab/
export type Curry <P extends any[], R> = (
  <T extends any[]> (...params: Cast <T, Partial <P>>) =>
  DropN <T['length'], P> extends [any, ...any[]]
    ? Curry <Cast <DropN <T['length'], P>, any[]>, R>
    : R
);
